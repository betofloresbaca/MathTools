# -*- coding: utf-8 -*-
'''Defines the necesary classes for working with Turing Machines'''

#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------

import copy
import sys


#-------------------------------------------------------------------------------
#                               Exceptions
#-------------------------------------------------------------------------------

class MAlgorithmException(Exception):
    '''Base class for the MAlgorithm Exceptions'''
    pass


class NoMoreReplacesException(MAlgorithmException):
    """The MAlgorithm has no more aplicable rules"""

    def __str__(self):
        '''Return the string representation of the halt'''
        return "El Algoritmo de Markov no tiene mas reemplazos posibles"


class HaltRuleException(MAlgorithmException):
    """The MAlgorithm has no more aplicable rules"""

    def __init__(self, rN):
        self.rN = rN

    def __str__(self):
        '''Return the string representation of the halt'''
        return "El Algoritmo de Markov llego a la regla final P" + str(self.rN)



#-------------------------------------------------------------------------------
#                               Main Classes
#-------------------------------------------------------------------------------


class MAlgorithm():

    def __init__(self):
        self.rules = []
        self.cad = ""
        self.lam = "x"
        self.haltSet = set()

    def loadRules(self, fileName):
        with open(fileName,"r") as fp:
            #Leyeendo el simbolo para lambda y el numero de reglas
            line = fp.readline()
            fields = line.split()
            self.lam = fields[0]
            n = int(fields[1])
            for k in range(n):
                line = fp.readline()
                fields = line.split()
                self.rules.append(fields[:2])
                if len(fields)==3 and fields[2]=="h":
                    self.haltSet.add(k+1)
            #print(self.rules)
            #print(self.lam)
            #print(self.haltSet)

    def execStep(self):
        for k in range(len(self.rules)):
            oldsubstr = self.rules[k][0] 
            newsubstr = self.rules[k][1].replace(self.lam,"")
            newCad = self.cad.replace(oldsubstr, newsubstr, 1)
            if newCad==self.cad:
                if k==(len(self.rules)-1):
                    raise NoMoreReplacesException()
                else:
                    continue
            self.cad = newCad
            if self.cad[0] != self.lam:
                self.cad = self.lam + self.cad
            if self.cad[-1] != self.lam:
                self.cad += self.lam
            print("Regla P", str(k+1), " ", self.rules[k][0],
                  " --> ", self.rules[k][1], "\t\t", self.getCad(),sep="")
            if (k+1) in self.haltSet:
                raise HaltRuleException(k+1)
            break

    def compute(self, cad=""):
        self.cad = self.lam + cad + self.lam
        #Imprimiendo la cadena inicial
        print("Cadena inicial:", self.getCad())
        while True:
            try:
                self.execStep()
            except MAlgorithmException as e:
                print("Cadena final:", end=" ")
                fCad = self.getCad() 
                if fCad == "":
                    fCad = self.lam
                print(fCad)
                print(e)
                break
            #input()

    def getCad(self):
        return self.cad.replace(self.lam, "")


#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

def main():
    if len(sys.argv) < 2:
        print("Forma de uso:")
        print("\tpython3 malgorithm.py <rules.ma> [cadEntrada.cad]")
        return
    ma = MAlgorithm()
    ma.loadRules(sys.argv[1])
    while True:
        line = input("MA>> ")
        if line == "QUIT":
            break
        ma.compute(line)

if __name__ == "__main__":
    main()