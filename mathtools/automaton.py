# -*- coding: utf-8 -*-
'''Defines the necesary classes for working with Finite Automatons'''


#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------
import sys
import collections #For check if a obj is iterable

#-------------------------------------------------------------------------------
#                               Exceptions
#-------------------------------------------------------------------------------

class AutomatonError(Exception):
    '''Base class for the Automaton Module Exceptions'''
    pass


class SymbolNotInAlphError(AutomatonError):
    """The symbol is not in the FiniteAutomata alphabet"""
        
    def __init__(self, symbol):
        '''Saves the simbol that causes the error'''
        self.symbol = symbol

    def __str__(self):
        '''Return the string representation of the error'''
        return "The symbol '" + str(self.symbol) + "' isn't in the alphabet."


class AnyTransitionForSymbol(AutomatonError):
    '''The automata has not transitions for the current symbol'''

    def __init__(self, symbol):
        '''Saves the simbol that causes the error'''
        self.symbol = symbol

    def __str__(self):
        '''Return the string representation of the error'''
        return "The actual states automaton have not transitions for" +\
                " the symbol '" + str(self.symbol) + "'"


#-------------------------------------------------------------------------------
#                               Main Classes
#-------------------------------------------------------------------------------

class State:
    '''Represents state of the Graph'''

    def __init__(self, name):
        '''Make the State named name'''
        self.name = name
        self.__connections = dict()

    def getStatesConnectedByPrice(self, price=0):
        '''Return a set of states to go from the actual state paying price'''
        if price in self.__connections:
            return self.__connections[price]
        else:
            return set()

    def getAllStatesConnected(self):
        conj = set()
        for s in self.__connections.values():
            conj |= s
        return conj

    def getConnections(self):
        return self.__connections

    def getMinPriceToState(self, stNm):
        for p in sorted(self.__connections.keys()):
            for s in self.__connections[p]:
                if s.name==stNm:
                    return p
        return -1

    def addConnection(self, to, price='\0'):
        '''Add a coonnection to a state, x.addConnection(next, <price>)'''
        if price in self.__connections:
            for st in self.__connections[price]:
                if st.name == to.name:
                    return False
        else:
            self.__connections[price] = set()
        self.__connections[price].add(to)
        return True

    def __lt__(self,other):
        '''Used for sorter purposes'''
        return self.name < other.name

    def __str__(self):
        '''Return the string representation of the State'''
        cad = "[ " + self.name
        for price in sorted(self.__connections):
            cad += ", "
            if price == '\0':
                cad += 'λ'
            else:
                cad += str(price)
            cad += "=>("
            for st in self.__connections[price]:
                cad += " " + str(st.name)
            cad += " ) "
        cad += "]"
        return cad


class Graph:
    '''Represents a graph witch can be used in several applications'''

    def __init__(self, statesNm, transitionsNm, directed=False, weighted=False):
        '''Initializes the Graph for containing the States interconnecteds'''
        #Making the states dic
        self.__states = {}
        #Prosesing the states set
        for stNm in statesNm:
            self.addState(stNm)
        #Procesing the transitions
        if weighted: #If weighted
            for fromSt,toSt,price in transitionsNm:
                #If we have a collection of prices
                if isinstance(price, collections.Iterable):
                    for p in sorted(price):
                        self.addStateConnection(fromSt, toSt, p)
                        if not directed:
                            self.addStateConnection(toSt, fromSt, p)
                else:
                    self.addStateConnection(fromSt, toSt, price)
                    if not directed:
                        self.addStateConnection(toSt, fromSt, price)
        else: #If not weighted
            for fromSt,toSt in transitionsNm:
                self.addStateConnection(fromSt, toSt)
                if not directed:
                    self.addStateConnection(toSt, fromSt)

    def addState(self, stateName):
        '''Add a state to the Graph, it creates a new State'''
        if not stateName in self.__states:
            self.__states[stateName] = State(stateName)
            return True
        else:
            return False

    def getState(self, stateName):
        '''Return the state that is named stateName from the Graph'''
        if not stateName in self.__states:
            return None
        return self.__states[stateName]

    def addStateConnection(self, fromStNm, toStNm, price='\0'):
        '''Add a inter states connection from->to with the symbol price
           if the states don't are in the Graph they are created'''
        if fromStNm not in self.__states:
            self.__states[fromStNm] = State(fromStNm)
        if toStNm not in self.__states:
            self.__states[toStNm] = State(toStNm)
        self.__states[fromStNm].addConnection(self.__states[toStNm], price)

    def printS(self, S):
        print("S = { ",end='')
        for s in sorted(S, key=lambda x:x[0]):
            print("(",s[0].name,",",s[1].name,")",end=" ")
        print("}",end='   ')

    def printW(self, W):
        print("W = { ",end='')
        for w in sorted(W, key=lambda x:x.name):
            print(w.name,end=" ")
        print("}")

    def printMinSpanningTree_Prim(self, beginStNm):
        if not beginStNm in self.__states:
            print('El estado no esta en el grafo')
            return
        S = set() #Aristas del arbol
        W = {self.getState(beginStNm)} #Nodos del arbol
        V = set(self.__states.values()) #Vertices del grafo
        it=0
        print("Iteración",it)
        self.printS(S)
        self.printW(W)
        while W != V:
            it+=1
            #Conjunto de solo conecciones salientes
            conjSoloExt = [(wSt,y) for wSt in W for y in wSt.getAllStatesConnected() if y in V-W]
            iMin,conMin=0,10000
            for i in range(len(conjSoloExt)):
                #print("Probando:",conjSoloExt[i][0].name,"-->",conjSoloExt[i][1].name)
                temp=conjSoloExt[i][0].getMinPriceToState(conjSoloExt[i][1].name)
                #print("  price:",temp)
                if temp!=-1 and temp<conMin:
                    iMin=i
                    conMin=temp
            S.add(conjSoloExt[iMin])
            W.add(conjSoloExt[iMin][1])
            print("Iteración",it)
            self.printS(S)
            self.printW(W)

    def __str__(self):
        '''Return the string representation of the Graph'''
        cadena = "{"
        for state in sorted(self.__states.values()):
            cadena += "\n\t" + str(state)
        cadena += "\n }"
        return cadena


class FiniteAutomata:
    '''FiniteAutomata: A finite automata representation
       Represents a mathematic Finite Automata
       Interface:
        x = FiniteAutomata(**args)
        x.evaluate(cad)
    '''

    def __init__(self, statesNm, alph, transitionsNm, initStNm, endStsNm):
        '''Initializes the FiniteAutomata using the mathematical notation
            statesNm -> Iterable of strings that are the name of the states
            alph -> Set of the alphabet for the FiniteAutomata
            transitionsNm -> List of tuples (fromStateName,price,toStateName)
                             where price could be a set of symbols
            initStNm -> Name of initial state
            endStsNm -> Iterable containing the end states name
        '''
        #Making the own Graph
        self.__graph = Graph(statesNm, transitionsNm, True, True)
        #Procesing the alphabet
        self.__alphabet = alph
        #Procesig the init state
        self.__initState = self.__graph.getState(initStNm)
        #Procesing the end states
        self.__endStates = set()
        for stNm in endStsNm:
            self.__endStates.add(self.__graph.getState(stNm))
        #Setting the actual states to void
        self.__actStates = {self.__initState} # A set

    def __lambdaClosure(self):
        '''Do the lambda closure over the Actual States Set'''
        auxSet = set()
        while True:
            prevLen = len(self.__actStates)
            for st in self.__actStates:
                auxSet |= st.getStatesConnectedByPrice('\0')#Lambda connections
            self.__actStates |= auxSet
            if prevLen == len(self.__actStates):
                break

    def transite(self, price):
        '''Transite over the include Graph using the price symbol'''
        if price not in self.__alphabet:
            raise SymbolNotInAlphError(price)
        self.__lambdaClosure()
        if price != '\0':
            newSet = set()
            for st in self.__actStates:
                newSet |= st.getStatesConnectedByPrice(price)
            if len(newSet) == 0:
                raise AnyTransitionForSymbol(price)
            self.__actStates = newSet
            self.__lambdaClosure()

    def reset(self):
        '''Resets the Actual States to the initial state'''
        self.__actStates.clear()
        self.__actStates.add(self.__initState)

    def evaluate(self, cad):
        '''Evaluates the cad with the actual Finite Automata'''
        if not isinstance(cad, str):
            raise TypeError("The type for cad to evaluate must be str")
        self.reset()
        try:
            for symbol in cad:
                self.transite(symbol)
        except AutomatonError:
            return False
        else:
            if len(self.__actStates&self.__endStates) == 0:
                return False
            else:
                return True
            
    def __str__(self):
        '''Return the string representation of the FiniteAutomata'''
        cad = "Finite Automaton:\n{\nAlphabet: "
        cad += str(sorted(self.__alphabet)) + "\nInit State: "
        cad += self.__initState.name + "\nEnd States:"
        for st in self.__endStates:
            cad += " " + st.name
        cad += "\nActual States:"
        for st in sorted(self.__actStates):
            cad += " " + st.name
        cad += "\nTransitions: "
        cad += str(self.__graph)
        return cad


#-------------------------------------------------------------------------------
#                               Unit Tests
#-------------------------------------------------------------------------------

import unittest

class TestFiniteAutomata(unittest.TestCase):
    '''Class for make the unit tests'''

    @classmethod
    def setUpClass(cls):
        #Making the first automata self.fa_1 for recognize of integers
        signs = {'+', '-'}
        digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}
        sts_1 = {'A', 'B', 'C'}
        initSt_1 = 'A'
        endSts_1 = {'C'}
        trans_1 = (('A', 'B', signs|{'\0'}), ('B', 'C', digits),\
                    ('C', 'C', digits))
        cls.finiteAutomata_1 = FiniteAutomata(sts_1, digits|signs,\
                                               trans_1, initSt_1, endSts_1)
        #Making the second automata self.fa_2 that recognize strings made by 
        # (d)^*1(d?)
        bits = {'0', '1'}
        sts_2 = {'A', 'B', 'C', 'D'}
        initSt_2 = 'A'
        endSts_2 = {'D'}
        trans_2 = (('A', 'B', bits|{'\0'}), ('B', 'A', {'\0'}),\
                   ('B', 'C', '1'), ('C', 'D', bits))
        cls.finiteAutomata_2 = FiniteAutomata(sts_2, bits, trans_2,\
                                               initSt_2, endSts_2)
        
    def test_transite(self):
        #For the integers finite automata
        self.finiteAutomata_1.reset()
        self.assertRaises(SymbolNotInAlphError,\
                          self.finiteAutomata_1.transite, "t")
        self.finiteAutomata_1.transite('0')
        self.assertRaises(AnyTransitionForSymbol,\
                          self.finiteAutomata_1.transite, "+")
        #For the second automata
        self.finiteAutomata_2.reset()
        self.assertRaises(SymbolNotInAlphError,\
                          self.finiteAutomata_2.transite, "t")

    def test_evaluate(self):
        #For the integers automata
        self.assertTrue(self.finiteAutomata_1.evaluate("-90121282"),\
                        "'-90121282' is a valid integer")
        self.assertFalse(self.finiteAutomata_1.evaluate("89+"),\
                        "'89+' isn't a valid integer")
        self.assertFalse(self.finiteAutomata_1.evaluate("+-9"),\
                        "'+-9' isn't a valid integer")
        #For the second automata
        self.assertTrue(self.finiteAutomata_2.evaluate("0010"),\
                        "'0010' has 1b in the end")
        self.assertFalse(self.finiteAutomata_2.evaluate("1001"),\
                        "'1001' has not 1b in the end")
        self.assertFalse(self.finiteAutomata_2.evaluate("1001011100"),\
                        "'1001011100' has not 1b in the end")

    @classmethod
    def tearDownClass(cls):
        del cls.finiteAutomata_1
        del cls.finiteAutomata_2
#-------------------------------------------------------------------------------
#                               Some tests
#-------------------------------------------------------------------------------
def testPrim():
    graph = Graph({'a','b','c','d','e','f','g'},\
        (('a','b',1),('b','c',2),('c','d',1),('d','e',2),('e','f',1),('f','a',2),\
        ('g','a',3),('g','b',2),('g','c',1),('g','d',3),('g','e',2),('g','f',1)),False,True)
    #print(graph.getState('e').getAllStatesConnected().pop())
    graph.printMinSpanningTree_Prim(sys.argv[1])
    #print(graph.getState("a").getMinPriceToState("g"))

#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    unittest.main()
    #testPrim()
