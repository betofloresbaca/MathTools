# -*- coding: utf-8 -*-
'''Unbounded register machine'''


#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------
import sys

#-------------------------------------------------------------------------------
#                               Main Classes
#-------------------------------------------------------------------------------

class Var:
    """Representa una vaariable"""

    def __init__(self, val = -1): #Por defecto se tiene un valor invalido
        self.val = val

    def __str__(self):
        return str(self.val)


#-------------------------------------------------------------------------------
#                               Basic functions
#-------------------------------------------------------------------------------

def succ_var(x):
    """Sucesor"""
    return Var(x.val + 1)

def pred_var(x):
    """Predecesor"""
    if x.val > 0:
        yv = x.val - 1
    else:
        yv = 0
    return Var(yv)

def var_assign_zero(x):
    """Asignar cero"""
    x.val = 0

def var0_assignsucc_var1(x, y):
    """Asignar sucesor"""
    x.val = succ_var(y).val

def var0_assignpred_var1(x, y):
    """Asignar predecesor"""
    x.val = pred_var(y).val

def while_var_not_0_do(x, function, args):
    """While x!=0 do{function(args)}od"""
    while x.val != 0:
        function(*args)


#-------------------------------------------------------------------------------
#                               Derivate functions
#-------------------------------------------------------------------------------

def var0_assign_var1(x, y):
    """x:=y"""
    var0_assignsucc_var1(x, y)
    var0_assignpred_var1(x, x)

def var0_assignplus_var1(x, y):
    """x:=x+y"""
    z = Var()
    var0_assign_var1(z, y)
    #Lo que se debe hacer dentro del ciclo
    def f(xx, zz):
        var0_assignsucc_var1(xx, xx)
        var0_assignpred_var1(zz, zz)
    #Llamando al ciclo
    while_var_not_0_do(z, f, [x, z])


def var_assign_const(x, c):
    """x:=c"""
    #Poniendo a cero la variable a asignar
    var_assign_zero(x)
    y = Var(c)
    #Asignando la vriable
    var0_assignplus_var1(x, y)

def var0_assignminus_var1(x, y):
    """x:=x-y"""
    z = Var()
    var0_assign_var1(z, y)
    #Lo que se debe hacer dentro del ciclo
    def f(xx, zz):
        var0_assignpred_var1(xx, xx)
        var0_assignpred_var1(zz, zz)
    #Llamando al ciclo
    while_var_not_0_do(z, f, [x, z])

def var0_assignmul_var1(x, y):
    """x:=x*y"""
    z = Var()
    w = Var()
    var0_assign_var1(z, y)
    var0_assign_var1(w, x)
    var_assign_zero(x)
    #Lo que se debe hacer dentro del ciclo
    def f(xx, ww, zz):
        var0_assignplus_var1(xx, ww)
        var0_assignpred_var1(zz, zz)
    #Llamando al ciclo
    while_var_not_0_do(z, f, [x, w, z])

def if_var_not_0_then(x, f, args):
    """if x!=0 then{f(args)}"""
    z = Var()
    var0_assign_var1(z, x)
    def g(zz, ff, aargs):
        ff(*aargs)
        var_assign_zero(z)
    while_var_not_0_do(z, g, [z, f, args])

def var0_assign_var1_minus_var2(x, y, z):
    """x:=y-z"""
    var0_assign_var1(x, y)
    var0_assignminus_var1(x, z)

def var0_assignover_var1(x, y):
    """x:=x/y"""
    z = Var()
    w = Var()
    var0_assign_var1(z, x)
    var_assign_zero(x)
    def f(ww, xx, yy, zz):
        var0_assign_var1_minus_var2(ww, yy, zz)
        var0_assignminus_var1(zz, yy)
        var0_assignsucc_var1(x, x)
        if_var_not_0_then(ww, var0_assignpred_var1, [xx, xx])
    while_var_not_0_do(z, f, [w, x, y, z])


#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

if __name__ == "__main__":
    x = Var()
    y = Var()
    z = Var()
    var_assign_const(x, 24)
    var_assign_const(y, 3)
    var0_assign_var1(z, x)
    var0_assignover_var1(z, y)
    print("El resultado de ",str(x),"/",str(y)," es: ", str(z), sep="")