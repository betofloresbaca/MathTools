# -*- coding: utf-8 -*-
'''Defines the necesary classes for working with Turing Machines'''

#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------

import copy
import sys


#-------------------------------------------------------------------------------
#                               Exceptions
#-------------------------------------------------------------------------------

class TMachineException(Exception):
    '''Base class for the TMachine Module Exceptions'''
    pass


class HaltFinalStateException(TMachineException):
    """The Turing Machine when to a final state"""
        
    def __init__(self, st):
        '''Saves the thread and the final state that stopes the TMachine'''
        self.fSts = [st]

    def addState(self, st):
        self.fSts.append(st)

    def __str__(self):
        '''Return the string representation of the halt'''
        return "La TM llegó a los estados finales " + str(set(self.fSts))


class HaltWhitoutTransitionsException(TMachineException):
    """The Turing Machine halts due to not available transitions"""

    def __init__(self):
        '''Default constructor'''
        pass

    def __str__(self):
        '''Return the string representation of the halt'''
        return "The TMachine hasn't more transitions"


#-------------------------------------------------------------------------------
#                               Main Classes
#-------------------------------------------------------------------------------


class WorkScope():
    """A work scpe that represents a execution Thread"""

    def __init__(self, parentWS = None, numTapes = 1, numTracks = 1):
        if parentWS:
            self.tapes = copy.deepcopy(parentWS.tapes)
            self.headPositions = parentWS.headPositions[:]
            self.actState = parentWS.actState;
        else:
            self.tapes = [[[""]*numTracks]]*numTapes #The tapes
            self.headPositions = [0]*numTapes #The head positions in tapes
            self.actState = "" #The actual state

    #TESTED OK
    def readTapes(self):
        return [ self.tapes[c][self.headPositions[c]]
                for c in range(len(self.headPositions))]
    
    def getTape(self, ind):
        if ind>=0 and ind < len(self.tapes):
            return self.tapes[ind]
        else:
            return None

    def setTape(self, tape, ind):
        self.tapes[ind] = tape

    def getState(self):
        return self.actState;

    def setState(self, nSt):
        self.actState = nSt

    def doAction(self, action): #action[2]->newstate action[0]->swrite action[1]->movs
        #Writting in the tapes
        if not action:
            return
        #Escribiendo en la cinta
        for k in range(len(action[0])):
            self.tapes[k][self.headPositions[k]] = action[0][k][:]
        #Moving the head
        for k in range(len(action[1])):
            if action[1][k]=="L": #Moviendo a la izquierda
                if(self.headPositions[k]==0):
                    self.tapes[k].insert(0,[""]*len(self.tapes[k][0]))
                else:
                    self.headPositions[k]-=1
            elif action[1][k]=="R": #Moviendo a la derecha
                if(self.headPositions[k]==(len(self.tapes[k])-1)):
                    self.tapes[k].append([""]*len(self.tapes[k][0]))
                self.headPositions[k]+=1
        #Changing the actState
        self.actState = action[2]

    def __str__(self):
        nTapes = len(self.tapes)
        nTracks = len(self.tapes[0][0])
        s = ""
        for k in range(nTapes):
            #s += "\nTape "+str(k)+":\n"
            #imprimeindo cada cinta
            for l in range(nTapes):
                #s += "\t"
                for m in range(len(self.tapes[k])): #Para cada simbolo
                    #imprimiendo la cabeza de lectura
                    # cuando es necesario o los espacios correspondientes
                    if m == self.headPositions[k]:
                        if l == 0:
                            s += "<"+self.actState+">"
                        else:
                            s += " "*(len(self.actState)+2)
                    #imprimiendo el caracter
                    c = self.tapes[k][m][l]
                    if c == "":
                        c = "B"
                    s += c
                #Si falta el B final
                if s[-1] != "B":
                    s += "B"
                #Agregando el simbolo de computo
                s += "├─"
                if nTapes>1 and l == (nTapes-1):
                    s += "\n"+"-"*(len(self.tapes[k])+len(self.actState)+2)
                #s += "\n"
        return s

class TuringMachine:
    """The main class for turing machine"""

    def __init__(self, fName):
        self.workScopes = []
        self.fTrans = []
        self.numTapes = 0
        self.numTracks = 0
        with open(fName,"r") as fd:
            #Leyendo los atributos de configuracion
            line = fd.readline()
            fields = line.split()
            self.numTapes = int(fields[0])
            self.numTracks = int(fields[1])
            numTF = int(fields[2])
            self.blank = fields[3]
            #Leyendo los estados inicial y final
            self.beginST = fd.readline().split()[0]
            self.endStates = set(fd.readline().split())
            #Leyendo las transiciones
            for line in fd:
                fields = line.split()
                fromSt = fields[0]
                toSt = fields[-1]
                rw = fields[1:-(1+self.numTapes)]
                rw = list(map(lambda x: list(map(lambda y: "" if y==self.blank else y,list(x))),rw))
                reads = rw[:self.numTapes]
                writes = rw[self.numTapes:]
                moves = fields[-(1+self.numTapes):-1]
                self.fTrans.append([
                    fromSt, #El estado actual
                    reads,  #Lo que debe leer en la cinta
                    writes, #Lo que escribe en la cinta
                    moves,  #La lista de movimientos
                    toSt    #El estado al que debe moverse
                ])
        ws = WorkScope(numTapes=self.numTapes)
        ws.setState(self.beginST)
        self.workScopes.append(ws)

    def loadTapes(self, fName):
        with open(fName,"r") as fd:
            #Creando el workscope inicial
            ws = WorkScope(numTapes=self.numTapes)
            #Leyendo una linea
            for line in fd:
                fields = line.split()
                numTape = int(fields[0])
                numTrack = int(fields[1])
                cont = list(fields[2])
                tape = ws.getTape(numTape)
                k = 0
                #Poniendo donde hay campos
                while k<len(cont) and k<len(tape):
                    tape[k][numTrack] = cont[k]
                    k+=1
                #Creando los campos necesarios
                while k<len(cont):
                    part = [""]*self.numTracks
                    part[numTrack] = cont[k]
                    tape.append(part)
                    k+=1
            ws.setState(self.beginST)
            #Agregando el primer hilo
            self.workScopes[0] = ws

    def findTransitions(self, actState, tapeSymbols):
        '''Find the transitions for the actua State and the readed symbols'''
        trans = list(map(lambda x: x[2:],
                         filter(lambda x: (x[0]==actState and x[1]==tapeSymbols),
                                self.fTrans)))
        if len(trans)>0:
            return trans
        return None

    def execStep(self):
        exeptFS = None
        newWS = [] #Workscopes que sobreviviran a la siguiente etapa
        for k in range(len(self.workScopes)):
            wsAct = [self.workScopes.pop(k)] #Sacando el workspace requerido
            st = wsAct[0].getState()
            reads = wsAct[0].readTapes()
            actions = self.findTransitions(st,reads)
            if actions!=None:
                #Creando los ws faltantes
                for l in range(len(actions)-1):
                    wsAct.append(WorkScope(wsAct[0]))
                #Ejecutando una accion en el cada ws
                for m in range(len(actions)):
                    wsAct[m].doAction(actions[m])
                    nSt = wsAct[m].getState()
                    if nSt in self.endStates:
                        if exeptFS:
                            exeptFS.addState(sSts)
                        else:
                            exeptFS = HaltFinalStateException(nSt)
                #Agregando los workscopes que siguen vigentes
                for w in wsAct:
                    newWS.append(w)
        #Haciendo el swap con los nuevos ws
        self.workScopes = newWS
        #Si se acabaron las transiciones
        if len(self.workScopes) == 0:
            raise HaltWhitoutTransitionsException()
        #Si se llego a un estado final
        if exeptFS:
            raise exeptFS;

    def compute(self, fName = None):
        executing = True
        if fName != None:
            self.loadTapes(fName)
        #imprimiendo el estado inicial
        print(self)
        while True:
            try:
                self.execStep()
                print(self)
            except TMachineException as e:
                print(self)
                print(e)
                break
            #input()

    def __str__(self):
        s = ""
        if len(self.workScopes) > 1:
            s += "{\n"
        for k in range(len(self.workScopes)):
            if len(self.workScopes) > 1:
                s += "\nThread: "+k+"\n"
            s += str(self.workScopes[k])
        if len(self.workScopes) > 1:
            s += "\n}"
        return s



#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

def main():
    if len(sys.argv) < 2:
        print("Forma de uso:")
        print("\tpython3 tmachine.py <maquina.tm> [cadEntrada.cad]")
        return
    t = TuringMachine(sys.argv[1])
    if len(sys.argv) > 2:
        t.compute(sys.argv[2])
    else:
        t.compute()

if __name__ == "__main__":
    main()