# -*- coding: utf-8 -*-
'''Defines the necesary classes for working with Turing Machines'''

#-------------------------------------------------------------------------------
#                               Imports
#-------------------------------------------------------------------------------

import sys

#-------------------------------------------------------------------------------
#                               Auxiliares
#-------------------------------------------------------------------------------

def val(x):
    return (str(x), x)


#-------------------------------------------------------------------------------
#                               Basicas
#-------------------------------------------------------------------------------

def zero(x):
    sX, iX = x
    sO, iO = val(0)
    return ("zero(" + sX + ")->" + sO, iO)

def succ(x):
    sX, iX = x
    sO, iO = val(iX+1)
    return ("succ(" + sX + ")->" + sO, iO)

def project(pos, *args):
    return ("project" + str(pos) + "(" +\
            ", ".join(list(map(lambda x: x[0], args))) + ")->" + str(args[pos - 1][1]),
            args[pos - 1])


#-------------------------------------------------------------------------------
#                               Elementales
#-------------------------------------------------------------------------------

def identity(x):
    sX, iX = x
    sO, iO = project(1, val(iX))
    return ("identity(" + sX + ")->" + sO, iO)

def two(x):
    sX, iX = x
    sO, iO = succ(succ(zero(val(iX))))
    return ("two(" + sX + ")->" + sO, iO)

def one(x, y):
    sX, iX = x
    sY, iY = y
    sO, iO = project(2, val(iX), succ(zero(val(iY))))
    return ("one(" + sX + ")->" + sO, iO)

def add(x, y):
    sX, iX = x
    sY, iY = y
    if iY == 0:
        sO, iO = x
    else:
        sO, iO = succ(add(val(iX), val(iY-1)))
    return ("add(" + sX + ", " + sY + ")->" + sO, iO)

def pred(x):
    sX, iX = x
    if iX == 0:
        sO, iO = val(0)
    else:
        sO, iO = val(iX - 1)
    return ("pred(" + sX + ")->" + sO, iO)

def monus(x, y):
    sX, iX = x
    sY, iY = y
    if iY == 0:
        sO, iO = x
    else:
        sO, iO = pred(monus(val(iX), val(iY-1)))
    return ("monus("+ sX + ", " + sY + ")->" + sO, iO)

def prod(x, y):
    sX, iX = x
    sY, iY = y
    if iY == 0:
        sO, iO = val(0)
    else:
        sO, iO = add(x, prod(val(iX), val(iY-1)))
    return ("prod(" + sX + ", " + sY + ")->" + sO, iO)

def div(x, y):
    sX, iX = x
    sY, iY = y
    if iY == 0:
        sO, iO = val(0)
    else:
        sO, iO = succ(div(val(iX), monus(succ(val(iY-1)), val(iX))))
    return ("div(" + sX + ", " + sY + ")->" + sO, iO)

#-------------------------------------------------------------------------------
#                               Relacionales
#-------------------------------------------------------------------------------

def sign(x):
    sX, iX = x
    if iX == 0:
        sO, iO = val(0)
    else:
        sO, iO = val(1)
    return ("sign(" + sX + ")->" + sO, iO)

def less(x, y):
    sX, iX = x
    sY, iY = y
    sO, iO = sign(monus(val(iY), val(iX)))
    return ("less(" + sX + ", " + sY + ")->" + sO, iO)

def greater(x, y):
    sX, iX = x
    sY, iY = y
    sO, iO = sign(monus(val(iX), val(iY)))
    return ("greater(" + sX + ", " + sY + ")->" + sO, iO)

def eq(x, y):
    sX, iX = x
    sY, iY = y
    sO, iO = monus(val(1), add(less(val(iX), val(iY)), less(val(iY), val(iX))))
    return ("eq(" + sX + ", " + sY + ")->" + sO, iO)

def noteq(x, y):
    sX, iX = x
    sY, iY = y
    sO, iO = monus(val(1), eq(val(iX), val(iY)))
    return ("noteq(" + sX + ", " + sY + ")->" + sO, iO)

#-------------------------------------------------------------------------------
#                               Main
#-------------------------------------------------------------------------------

def main():
    print(zero(val(5))[0], end="\n\n")
    print(succ(val(5))[0], end="\n\n")
    print(project(2,val(5),val(9),val(1))[0], end="\n\n")
    print(identity(val(5))[0], end="\n\n")
    print(two(val(5))[0], end="\n\n")
    print(one(val(5),val(9))[0], end="\n\n")
    print(add(val(5),val(3))[0], end="\n\n")
    print(pred(val(5))[0], end="\n\n")
    print(monus(val(5),val(3))[0], end="\n\n")
    print(prod(val(2),val(3))[0], end="\n\n")
    print(div(val(2),val(4))[0], end="\n\n")
    print(sign(val(5))[0], end="\n\n")
    print(less(val(5),val(3))[0], end="\n\n")
    print(greater(val(5),val(3))[0], end="\n\n")
    print(eq(val(2),val(2))[0], end="\n\n")
    print(noteq(val(2),val(2))[0], end="\n\n")
    
if __name__ == "__main__":
    main()